#!/bin/bash

#######################################################
#                                                     #
# GPO Pick Up Ebuilds script -> G PUE -> Ireek/Greek  #
# In french "je pue" stands for "I reek"              #
# Licenced under terms of the N.W.I.U.L.F.T.S licence #
#                                                     #
#######################################################
#                                                     #
# No Way I Use A Licence For This Script              #
# I you use it please be pleased, or not              #
# A mention to all contributors should be in header   #
# Only change allowed in header is adding your name   #
# At the end of contributors list when script changed #
#                                                     #
#######################################################
#                                                     #
#                  CONTRIBUTORS                       #
#      (mazes_80) nodresszesam@hotmail.com            #
#                                                     #
#######################################################

# TODO check ebuild dependencies satisfaction through portage helpers
# TODO loose match (or completely skip it)
# TODO store successful actions count (but why ?)

# Configuration
declare -r serv_url='http://gpo.zugaina.org'	# Server url
declare -r ireekrc=${IREEKRC:-.ireek}			# RC file
declare -ri dbglvl=${DEBUG:-0}

# Command line switches
declare -i act_build=0
declare -i act_fetch_d=0
declare -i act_fetch_e=0
declare -i act_view=0
declare -i act_list_o=0
declare -i act_list_e=0

declare -i opt_interactive=0
declare -i opt_loose_match=0

# Datas
declare -a ebuild_list		# Ebuild array with info
							# cat-gory/ebuild-version;URL;overlay
declare -a match_list		# Ebuild matching name
declare -a desc_list		# Description by ebuild name
declare -i action_count=0
declare -i query_count=0

declare ebuild_category
declare ebuild_name
declare ebuild_version
declare overlay_name

# usage: displays help and quit
function usage() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	echo 'ireek [ -b ] [ -c category ] [ -e ebuild ] [ -h ] [ -f ]'
	echo '      [ -F ] [ -n ebuild_version ] [ -o overlay ] [ -V ]'
	echo '      [ -i ] [ -l ] [ -L ]'
	echo '      [ ebuild ] [ overlay ] [ ebuild_version ]'
	echo ''
	echo '      Action switches'
	echo '         -b build or rebuild current pick up overlay'
	echo '            needs .ireek to exist in OVERDIR'
	echo '            or current directory if not specified'
	echo '            structure 2 fields: ebuild and overlay name'
	echo '            exclusive option, skips everything else'
	echo '         -f fetch ebuild needs overlay, ebuild and ebuild version'
	echo '         -F fetch ebuild directory needs overlay and ebuild'
	echo '         -h display this help'
	echo '         -l list overlays containing ebuild'
	echo '         -L list matching ebuild'
	echo '            default if no action'
	echo '         -V view ebuild needs overlay, ebuild and ebuild version'
	echo ''
	echo '      Data switches'
	echo '      overided by positional parameters'
	echo '         -c category'
	echo '            overided when ebuild name is fully qualified'
	echo '         -n ebuild version'
	echo '         -o overlay strict name'
	echo '         -e ebuild name'
	echo ''
	echo '      Options'
	echo '         -m match ebuild name "loosely"'
	echo ''
	echo '      Environment'
	echo '         OVERDIR: Overlay directory, defaults to current working directory'
	echo '         IREEKRC: Configuration file name, defaults ".ireekrc"'
	echo '                  This script seeks this file in the overlay directory'
	echo '         DEBUG: Numeric value: if not -> 0'
	echo '                >=1 display execution thread'
	echo '                >=2 use lastpipe while assigning requests array'

	exit 0
}

# error_message <message> [leave]
# leave: boolean. if 0 then finish
function error_message() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	echo -e "${1}" >/dev/stderr
	[ -n "${2}" ] && [ ${2} -eq 1 ] && exit 1
}

function match_list_reinit() {
	declare -i idx=0

	for item in ${match_list[@]}; do
		desc_list[${idx}]=${item#*;}
		desc_list[${idx}]=${desc_list[${idx}]//§/ }
		match_list[${idx}]=${item%%;*}
		(( idx++ ))
	done
}

# ebuild__query
# Query wrapper
function ebuild__query() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	declare -i idx

	(( query_count++ ))

	# Split ebuild and category if provided
	[ "${ebuild_name%/*}" != "${ebuild_name}" ] \
		&& ebuild_category="${ebuild_name%/*}" \
		&& ebuild_name="${ebuild_name#*/}"
	# TODO empty ebuild (for multiple action)
	# TODO ebuild name syntax check for now assume syntax is good

	if [ ${dbglvl} -lt 2 ]; then
		# Query server for matching ebuild names
		match_list=($(ebuild__query_match))
		[ ${#match_list[@]} -eq 0 ] \
			&& query_failure ${1}
		match_list_reinit
		# And for an ebuild list
		ebuild_list=($(ebuild__query_list))
	else
		idx=0
		local obj

		# Save bash state
		set -o | grep 'monitor on'
		declare -r monitor_state=${?}
		shopt lastpipe | grep -q off
		declare -r -i lastpipe_state=${?}

		# Set bash state
		shopt -s lastpipe
		set +o monitor

		ebuild__query_match | while read obj; do
			match_list[idx++]="${obj}"
		done
		[ ${#match_list[@]} -eq 0 ] \
			&& query_failure ${1}
		match_list_reinit

		idx=0
		ebuild__query_list | while read obj; do
			ebuild_list[idx++]="${obj}"
		done

		# Restore bash state
		[ ${monitor_state} -eq 0 ] \
			&& set -o monitor
		[ ${lastpipe_state} -eq 0 ] \
			&& shopt -u lastpipe
	fi

	# May keep old way
	# done after: ebuild__query_match
	# is enough ?
	[ ${#ebuild_list[@]} -eq 0 ] \
		&& query_failure ${1} \
		&& return 1
	return 0
	# Old way
	# ((${#ebuild_list[@]}>=0))
	# return ${?}
}

# ebuild__query_match
# Query gpo.zugaina.org about matching names
function ebuild__query_match() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	declare -r serv_search='Search?search='
	local to_match

	# TODO include ability to search in descriptions
	# Set matching pattern
	[ ${opt_loose_match} -eq 0 ] \
		&& to_match="\/${ebuild_name}" \
		|| to_match="\/.*${ebuild_name}[^;]*"
		#|| to_match="\/.*${ebuild_name}.*"
	[ -n "${ebuild_category}" ] \
		&& to_match="${ebuild_category}${to_match}"

	# Query and filter output
	curl -s ${serv_url}/${serv_search}${ebuild_name} \
	| sed -n "/[^^]<\/div><\/div>/ {
				s/^[^<]\+<div>//;
				s/ <div>/;/;
				s:</div></div>::;
				s/ /§/g;
				/${to_match};/p
			}"
	#| sed -n -e 's:^.*href="/\([^-]\+-[^-]\+/\):\1:' \
	#	-e 't SKIP; b;:SKIP' \
	#	-e 's/">$//' \
	#	-e "/${to_match}$/p"

	return 0
}

function ebuild__query_list() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	local package

	for package in ${match_list[@]}; do
		curl -s ${serv_url}/${package} \
		| sed -n -e "/>${package##*/}-/s:</b></div>$::" \
			-e 't SKIP; b;:SKIP' \
			-e "s:.*>:${package%/*}/:" \
			-e 'N;N;N;s/\n.*//g;N' \
			-e 's:\n.*href="\(/AJAX/Ebuild\):;\1:' \
			-e 's:".*/Overlays/:;:;s/".*//' \
			-e "/${overlay_name:-.}/p"
	done
	return 0
}

function query_failure() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	local error="No ebuild $([ $opt_loose_match -eq 1 ] && echo 'macthes' || echo 'named'): ${ebuild_name}"
	[ -n "${overlay_name}" ]  \
		&& error="${error} in overlay: ${overlay}"
	[ -n "${ebuild_version}" ] \
		&& error="${error} with version: ${ebuild_version}"
	error_message "$error" ${1}
	return 0
}

# Filter ebuild_list to keep only package in current overlay
function ebuild__list_filter_overlay() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	for item in ${ebuild_list[@]}; do
		echo ${item}
	done | grep "${overlay_name}"
}

function interactive_menu() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	declare -r overlay_list=$(list_matching_overlays)
	declare -r quit='Upper level' # Interactive menu
	local overlay package action

	overlay=''; while [ "${overlay}" != "${quit}" ];
	do echo "Select overlay";
	select overlay in ${overlay_list[*]} "Search other" "${quit}"; do
		[ "${overlay}" = "${quit}" ] && break

		overlay_name="${overlay}"

		package_list=$(ebuild__list_filter_overlay | sed 's/;[^ ]*//; s:^/::')

		package=''; while [ "${package}" != "${quit}" ];
		do echo "Select ebuild"
		# TODO refactor
		select package in ${package_list[@]} \
			"Fetch directory" \
			"Add to build list" \
			"${quit}"; do
			case "${package}" in
				"${quit}") break;;
				"Fetch directory")
					fetch_directory
					break;;
				"Add to build list")
					ebuild__add_to_config
					break;;
			esac

			# TODO loose match conditionnal
			ebuild_version="${package#*/}"
			ebuild_version="${ebuild_version/${ebuild_name}-/}"

			action=''; while [ "${action}" != "${quit}" ];
			do echo "Select task"
			# TODO loose match
			# if [ ${opt_loose_match} -eq 1 ]; then
			#     ebuild_name=${package}
			select action in "View" "Fetch" "${quit}"; do
				case ${action} in
					"View") fetch_ebuild | less;;
					"Fetch") fetch_ebuild > ${ebuild_name}-${ebuild_version}.ebuild;;
				esac
			break; done; done;
		break; done; done;
	break; done; done;
}

# TODO Finish
function ebuild__add_to_config() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	sed -i "s/^[^\/]*${ebuild_name} /d" ${OVERDIR}${ireekrc} 2>/dev/null
	# echo "sed -i 's/^[^\/]*${ebuild_name} /d' ${OVERDIR}${ireekrc} 2>/dev/null" > stderr
	# echo ${ebuild_category} >> stderr
	echo "${ebuild_name} ${overlay}" >> ${OVERDIR}${ireekrc}
}

function ebuild__check_multiple_name() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	for item in ${ebuild_list[@]}; do
		echo ${item}
	done | sed 's:/[^ ]\+;:;:' | sort | uniq
}

# fetch_ebuild [args]
# fetch an ebuild
# args allow to output result elsewhere than tty
function fetch_ebuild {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	# TODO Refactor
	declare -r suffix=$(echo ${ebuild_list[@]} | sed "s/.*\([^ ]*${ebuild_version};[^ ]*${overlay_name}\).*/\1/" | cut -d';' -f2)

	(( action_count++ ))
	curl -s "${serv_url}/${suffix}/View" | sed '1s/^<[^>]*>//;/ajax rendered/d'
	return 0
}

# fetch_directory
# fetches complete ebuild tree
function fetch_directory() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	local category
	local filename
	local url

	(( action_count++ ))

	[ -z "${overlay_name}" ] \
		&& error_message "Option -F requires both ebuild and overlay to be declared"

	[ -n "${ebuild_category}" ] \
		&& category="${ebuild_category}" \
		|| {
			[ $(ebuild__check_multiple_name | wc -w) -ne 1 ] \
				&& error_message "Multiple match for ${ebuild_name}, select category in:\n$(for match in ${match_list[@]}; do echo "	${match%/*}"; done)" 1
			# ^ TODO interactive mode
			category="$(echo ${ebuild_list[0]} | sed 's:/.*::')"
		}
	url="${serv_url/gpo/data.gpo}/${overlay_name}/${category}/${ebuild_name}/"
	[ ${act_build} -eq 1 ] && \
		echo "Fetch ${category}/${ebuild_name} from overlay: ${overlay_name}"

	[ -e "${OVERDIR}${category}/${ebuild_name}" ] \
		&& rm -rf "${OVERDIR}${category}/${ebuild_name}"/* \
		|| mkdir -p "${OVERDIR}${category}/${ebuild_name}"

	for filename in $(curl -s ${url} | sed -n '/^<img/ {s:.*href="::;s/".*//;p}'; \
		curl -s ${url}files/ | sed -n '/^<img/ {s:.*href=":files/:;s/".*//;p}');
	do
		[ "${filename}" = "files/" ] && \
			mkdir -p "${OVERDIR}${category}/${ebuild_name}/files" || \
			curl -s -o "${OVERDIR}${category}/${ebuild_name}/${filename}" "${url}${filename}"
	done
	return 0
}

function build_overdir() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	local line;
	while read line; do
		ebuild_name=${line% *}
		overlay_name=${line#* }
		ebuild__query && \
			fetch_directory
	done < "${OVERDIR}${ireekrc}"
}

function list_matching_overlays() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	(( action_count++ ))

	echo ${ebuild_list[*]} \
		| sed 's/[^ ;]*;[^;]*;//g;s/ /\n/g' \
		| sort | uniq
	return 0
}

function list_matching_ebuild() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	(( action_count++ ))

	declare -i idx=0
	for item in ${match_list[@]}; do
		echo ${item}: ${desc_list[${idx}]}
		(( idx ++ ))
	done | sort

	return 0
}

function run_default() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	declare -i error=0

	[ ${act_fetch_d} -eq 1 ] \
		&& fetch_directory
	if [ ${act_fetch_e} -eq 1 ]; then
		[ -n "${overlay_name}" -a -n "${ebuild_version}" ] \
		&& fetch_ebuild > ${ebuild_name}-${ebuild_version}.ebuild \
		|| error=${error}+2
	fi
	if [ ${act_view} -eq 1 ]; then
		[ -n "${overlay_name}" -a -n "${ebuild_version}" ] \
		&& fetch_ebuild | less \
		|| error=${error}+4
	fi
	[ ${act_list_o} -eq 1 ] \
		&& list_matching_overlays
	[ ${act_list_e} -eq 1 ] \
		&& list_matching_ebuild

	[ ${action_count} -eq 0 ] \
		&& list_matching_ebuild \
		&& action_count=0

	(((${error}&2)==2)) && error_message "Option -f requires both ebuild, overlay and version to be declared"
	(((${error}&4)==4)) && error_message "Option -V requires both ebuild, overlay and version to be declared"

	return 0
}

function run() {
	[ ${dbglvl} -ge 1 ] && echo ${FUNCNAME[0]} > /dev/stderr
	# Parse command line
	while getopts "bfFhilLmn:o:e:V" opt; do
		case ${opt} in
			b) act_build=1;;
			f) act_fetch_e=1;;
			F) act_fetch_d=1;;
			h) usage;;
			l) act_list_o=1;;
			L) act_list_e=1;;
			V) act_view=1;;
			n) ebuild_version="${OPTARG}";;
			o) overlay_name="${OPTARG}";;
			e) ebuild_name="${OPTARG}";;
			i) opt_interactive=1;;
			m) opt_loose_match=1;;
			?) error_message "Invalid option -${OPTARG}";;
		esac
	done
	shift $(($OPTIND - 1))

	# Check OVERDIR validity
	if [ "${OVERDIR}" != "" ]; then
		[ "${OVERDIR: -1}" != '/' ]
			OVERDIR="${OVERDIR}/"
		[ ! -d "${OVERDIR}" ] && \
			unset OVERDIR
	fi

	if [ ${act_build} -eq 1 ]; then
		opt_loose_match=0
		build_overdir
	else
		# Read main datas from positional parameters
		[ -n "${1}" ] && ebuild_name="${1}"
		[ -z "${ebuild_name}" ] \
			&& error_message "Ebuild name required through switch or positionnal parameters" 1
		[ -n "${2}" ] && overlay_name="${2}"
		[ -n "${3}" ] && ebuild_version="${3}"

		ebuild__query 1
		[ ${opt_interactive} -eq 0 ] \
			&& run_default \
			|| interactive_menu
	fi

	return 0;
}

run ${@}

exit ${?}
